+ (NSInteger)buttonTagWithSection:(NSInteger)section row:(NSInteger)row
{
     
    NSString *tagString = [NSString stringWithFormat:@"%d%@",section,[TagInGroupTable stringForFourLengthwithNumber:row]];
    return [tagString integerValue];
}
 
+ (NSString*)stringForFourLengthwithNumber:(NSInteger)number
{
    if (number<10) {
        return [NSString stringWithFormat:@"000%d",number];
    }else if(number<100)
    {
        return [NSString stringWithFormat:@"00%d",number];
    }else if(number<1000)    
        return [NSString stringWithFormat:@"0%d",number];
    else
        return [NSString stringWithFormat:@"%d",number];
}
 
+ (NSIndexPath *)indexWithTag:(NSInteger )tag
{
    NSInteger length = [[NSString stringWithFormat:@"%d",tag] length];
    NSString *sectionString = [[NSString stringWithFormat:@"%d",tag] substringToIndex:length-4];
    NSString *rowString = [[NSString stringWithFormat:@"%d",tag] substringFromIndex:length-4];
    return [NSIndexPath indexPathForRow:[rowString integerValue] inSection:[sectionString integerValue]];
}